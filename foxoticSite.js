/**
  Main foxoticSite module, handles all the code...
 */
// Imports
const logger = require('winston');
const express = require('express');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const session = require('express-session');
const MongoStore = require('connect-mongo')(session);
const passport = require('passport');
const passportSocketIo = require('passport.socketio');
const mongoose = require('mongoose');
const httpServer = require('http');
const socketIo = require('socket.io');
const hbs = require('hbs');
const routes = require('./routes/index');

class FoxoticSite {
  constructor(config) {
    logger.info('[FoxoticSite] Initializing Express, Socket.io, Passport and Database');
    this.config = config;
    this.init(config);
  }

  init(config) {
    // Init all our server controls
    this.app = express();
    this.server = httpServer.Server(this.app);
    this.io = socketIo(this.server);
    this.passport = passport;
    this.mongoose = mongoose.connect(config.get('db').url);

    // Init express and passport stuff
    this.hbs = hbs;
    this.hbs.registerPartials(`${__dirname}/views/partials`);
    this.app.set('view engine', 'html');
    this.app.engine('html', this.hbs.__express);
    this.app.use(bodyParser.json());
    this.app.use(bodyParser.urlencoded({
      extended: false,
    }));
    this.app.use(cookieParser());
    this.mongoSessionStore = new MongoStore({ mongooseConnection: this.mongoose.connection });
    this.app.use(session({
      name: config.get('session').name,
      key: config.get('session').name,
      secret: config.get('session').secret,
      resave: config.get('session').resave,
      saveUninitialized: true,
      store: this.mongoSessionStore,
      cookie: { maxAge: config.get('session').maxAge },
    }));
    this.app.use(passport.initialize());
    this.app.use(passport.session());

    // Passport auth functions
    function onAuthorizeSuccess(data, accept) {
      logger.debug('[FoxoticSite] Socket user authorized!');
      accept();
    }
    function onAuthorizeFail(data, message, error, accept) {
      if (error) { accept(new Error(message)); logger.debug('[FoxoticSite] Socket user authorized!'); }
        // this error will be sent to the user as a special error-package
        // see: http://socket.io/docs/client-api/#socket > error-object
    }

    // Passport auth
    this.io.use(passportSocketIo.authorize({
      cookieParser,
      key: config.get('session').name,
      secret: config.get('session').secret,
      store: this.mongoSessionStore,
      success: onAuthorizeSuccess,
      fail: onAuthorizeFail,
    }));

    // Require Passport Controller, should be edited to not need this disable...
    require('./modules/passportCtrlr')(this.passport, this.config, this.mongoose); // eslint-disable-line global-require

    // New stuff
    this.app.use('/', routes);
    // This should be at end
    this.server.listen(config.get('server').port, () => {
      logger.info(`[FoxoticSite] Server listening on the port: ${config.get('server').port}`);
    });
  }
}

module.exports = FoxoticSite;
