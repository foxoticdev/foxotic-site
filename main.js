/*
    Entry file for foxoticsite, handles all the logging and init.
*/
const logger = require('winston');
const fs = require('fs');
const FoxoticSite = require('./foxoticSite');

const Config = require('./config/config');


/* ---LOGGING---*/
const now = new Date();
const t = now.toISOString().replace(/[:.]/gi, '-');
const fname = `./log/${t}.log`;
try {
  fs.mkdirSync('./log');
} catch (e) {
  logger.log('err', e);
}


logger.level = 'debug';

logger.remove(
    logger.transports.Console).add(logger.transports.Console, {
      colorize: true,
      handleExceptions: true,
      humanReadableUnhandledException: true,
    }).add(logger.transports.File, {
      level: 'debug',
      filename: fname,
      handleExceptions: true,
    }).handleExceptions(new logger.transports.File({
      filename: './crash.log',
    }));

const config = Config();


const foxoticSite = new FoxoticSite(config); // eslint-disable-line no-unused-vars
