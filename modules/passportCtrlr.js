const LocalStrategy = require('passport-local').Strategy;
const TwitchStrategy = require('passport-twitch').Strategy;
const User = require('../models/user');

function passportCtrlr(passport, config) {
  passport.serializeUser((user, done) => {
    done(null, user.id);
  });

  passport.deserializeUser((id, done) => {
    User.findById(id, (err, user) => {
      done(err, user);
    });
  });

  passport.use('local-signup', new LocalStrategy({
    usernameField: 'email',
    passwordField: 'password',
    passReqToCallback: true,
  },
    (req, email, password, done) => {
      process.nextTick(() => {
        User.findOne({
          'local.email': email,
        }, (err, user) => {
          if (err) { return done(err); }
          if (user) {
            return done(null, false, req.flash('signupMessage',
              'That email is already taken.'));
          }
          const newUser = new User();
          newUser.local.email = email;
          newUser.local.password = newUser.generateHash(
              password);
          newUser.save((err) => {
            if (err) { throw err; }
            return done(null, newUser);
          });
        });
      });
    }));

  passport.use('local-login', new LocalStrategy({
    usernameField: 'email',
    passwordField: 'password',
    passReqToCallback: true,
  },
    (req, email, password, done) => {
      User.findOne({
        'local.email': email,
      }, (err, user) => {
        if (err) { return done(err); }
        if (!user) {
          return done(null, false, req.flash('loginMessage',
            'No user found.'));
        }
        if (!user.validPassword(password)) {
          return done(null, false, req.flash('loginMessage',
            'Oops! Wrong password.'));
        }
        return done(null, user);
      });
    }));

  passport.use(new TwitchStrategy({
    clientID: config.get('twitchAuth').clientID,
    clientSecret: config.get('twitchAuth').clientSecret,
    callbackURL: config.get('twitchAuth').callbackURL,
    scope: 'user_read',
  },
    (token, refreshToken, profile, done) => {
      process.nextTick(() => {
        User.findOne({
          'twitch.id': profile.id,
        }, (err, user) => {
          if (err) { return done(err); }
          if (user) {
            return done(null, user);
          }
          const newUser = new User();
          newUser.twitch.id = profile.id;
          newUser.twitch.token = token;
          newUser.twitch.email = profile.email;
          newUser.twitch.username = profile.username;
          newUser.twitch.displayName = profile.displayName;
          newUser.twitch.partnered = profile._json.partnered;
          newUser.twitch.logo = profile._json.logo;
          newUser.twitch.raw = profile;
          newUser.save((err) => {
            if (err) { throw err; }
            return done(null, newUser);
          });
        });
      });
    }));
}
module.exports = passportCtrlr;
