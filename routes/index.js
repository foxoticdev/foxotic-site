const express = require('express');

const passport = require('passport');

function isLoggedIn(req, res, next) {
  if (req.isAuthenticated()) { return next(); }
  res.redirect('/');
}

const router = express.Router();

router.get('/', (req, res) => {
  res.render('index', {
    title: 'Express',
  });
});

/*
router.get('/login', (req, res) => {
  res.render('login.ejs', {
    message: req.flash('loginMessage'),
  });
});

router.get('/signup', (req, res) => {
  res.render('signup.ejs', {
    message: req.flash('signupMessage'),
  });
});*/

router.get('/profile', isLoggedIn, (req, res) => {
  res.render('profile', {
    user: req.user,
  });
});

router.get('/logout', (req, res) => {
  req.logout();
  res.redirect('/');
});

router.post('/signup', passport.authenticate('local-signup', {
  successRedirect: '/profile',
  failureRedirect: '/signup',
  failureFlash: true,
}));

router.post('/login', passport.authenticate('local-login', {
  successRedirect: '/profile',
  failureRedirect: '/login',
  failureFlash: true,
}));


router.get('/auth/twitch', passport.authenticate('twitch'));

router.get('/auth/twitch/callback', passport.authenticate('twitch', {
  successRedirect: '/profile',
  failureRedirect: '/',
}));


module.exports = router;
