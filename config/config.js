const convict = require('convict');
const path = require('path');

function configParser() {
  const config = convict({
    server: {
      port: {
        doc: 'Port of the server',
        format: 'int',
        default: 3000,
      },
    },
    db: {
      url: {
        doc: 'Connection url of the mongodb server',
        format: String,
        default: '',
      },
    },
    twitchAuth: {
      clientID: {
        doc: 'Oauth clientID',
        format: String,
        default: '',
      },
      clientSecret: {
        doc: 'Oauth clientSecret',
        format: String,
        default: '',
      },
      callbackURL: {
        doc: 'The callback url to our backend',
        format: String,
        default: '',
      },
    },
    session: {
      name: {
        doc: 'Session cookie name',
        format: String,
        default: '',
      },
      secret: {
        doc: 'The secret for all cookies, this should be a random string over 20 chars',
        format: String,
        default: '',
      },
      resave: {
        doc: 'Should the cookie be resaved on each request?',
        format: Boolean,
        default: true,
      },
      maxAge: {
        doc: 'The age of the cookie',
        format: 'int',
        default: 60000,
      },
    },
  });

    // Load configuration
  config.loadFile(path.resolve(__dirname, 'config.json'));

    // Perform validation
  config.validate();

  return config;
}
module.exports = configParser;

