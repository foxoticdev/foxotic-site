const mongoose = require('mongoose');
const bcrypt = require('bcrypt-nodejs');

const userSchema = mongoose.Schema({
  local: {
    username: String,
    email: String,
    password: String,
  },
  twitch: {
    id: String,
    token: String,
    email: String,
    username: String,
    displayName: String,
    partnered: Boolean,
    logo: String,
    raw: Object,
    infobox: String,
  },
  info: {
    role: String,
  },
});

userSchema.methods.generateHash = function generateHash(password) {
  return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

userSchema.methods.validPassword = function validPassword(password) {
  return bcrypt.compareSync(password, this.local.password);
};

module.exports = mongoose.model('user', userSchema);
